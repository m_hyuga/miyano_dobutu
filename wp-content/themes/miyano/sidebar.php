<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<div id="sub_navi" class="left">

<div class="sub_navi">
<img src="<?php bloginfo('template_url'); ?>/images/navi/img_01.jpg" alt="みやの動物病院">
<div class="navi_box">
<ul>
<li><a href="<?php bloginfo('url'); ?>/hospital/">病院のご案内</a></li>
<li><a href="<?php bloginfo('url'); ?>/schedule/">スタッフスケジュール</a></li>
<li><a href="<?php bloginfo('url'); ?>/noto/">能登診療所</a></li>
</ul>
</div>
</div>

<div class="sub_navi">
<img src="<?php bloginfo('template_url'); ?>/images/navi/img_02.jpg" alt="アニマルビューティサロンマック">
<div class="navi_box">
<ul>
<li><a href="<?php bloginfo('url'); ?>/salon/">アニマルビューティサロン<br />マック</a></li>
<li><a href="http://beautysalon-mac.seesaa.net">スタッフブログ</a></li>
</ul>
</div>
</div>

<div class="sub_navi">
<img src="<?php bloginfo('template_url'); ?>/images/navi/img_05.jpg" alt="新着・更新情報">
<div class="navi_box">
<?php switch_to_blog(1) ?>
<ul>
	<?php
	global $post;
	$myposts = get_posts('numberposts=5');
	foreach($myposts as $post) :
	setup_postdata($post);
	?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endforeach; ?>
	 
	<?php restore_current_blog(); ?>

</ul>
<div class="newS"><a href="http://miyano-animal.com/category/new/">一覧＞＞</a></div>
</div>
</div>


<div class="sub_navi">
<img src="<?php bloginfo('template_url'); ?>/images/navi/img_04.jpg" alt="院長コラム">
<div class="navi_box">

<?php RSSImport(3, "http://miyano-animal.seesaa.net/index.rdf",true,false); ?>



</div>
</div>


<div class="sub_navi">
<img src="<?php bloginfo('template_url'); ?>/images/navi/img_03.jpg" alt="よくある質問">
<div class="navi_box">
<?php switch_to_blog(5) ?>
<ul>
	<?php
	global $post;
	$myposts = get_posts('numberposts=3');
	foreach($myposts as $post) :
	setup_postdata($post);
	?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endforeach; ?>
	 
	<?php restore_current_blog(); ?>

</ul>
<div class="newS"><a href="http://miyano-animal.com/faq/">一覧＞＞</a></div>
</div>
</div>






</div>