<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<?php get_sidebar(); ?>

<div id="main" class="right">

<div class="index_contents">
<div class="middle_box left">
<img src="<?php bloginfo('template_url'); ?>/images/index/img_middle_header.jpg" class="header"><div class="middle_box_inner">
<div class="middle_box_contents"> 
<img src="<?php bloginfo('template_url'); ?>/images/index/img_02.jpg" alt="みやの動物病院">
<div class="middle_box_text"> 
<h2><img src="<?php bloginfo('template_url'); ?>/images/index/img_03.jpg" alt="みやの動物病院"></h2>
<p>金沢市近岡町にある「みやの動物病院」です。「愛情が一番の治療です」をモットーにスタッフ一同、丁寧に対応いたします。
どんな小さなことでもご相談下さい。</p>
</div>
<p class="right"><a href="http://miyano-animal.com/hospital/"><img src="<?php bloginfo('template_url'); ?>/images/index/img_05.jpg" alt="病院のご案内"></a></p>
<div class="clear"></div>
</div>
</div>
</div>

<div class="middle_box right">
<img src="<?php bloginfo('template_url'); ?>/images/index/img_middle_header.jpg" class="header"><div class="middle_box_inner">
<div class="middle_box_contents"> 
<img src="<?php bloginfo('template_url'); ?>/images/index/img_01.jpg" alt="能登診療所">
<div class="middle_box_text"> 
<h2><img src="<?php bloginfo('template_url'); ?>/images/index/img_04.jpg" alt="能登診療所"></h2>
<p>みやの動物病院「能登診療所」のご案内です。</p>
</div>
<p class="right"><a href="http://miyano-animal.com/noto/"><img src="<?php bloginfo('template_url'); ?>/images/index/img_05.jpg" alt="病院のご案内"></a></p>
<div class="clear"></div>
</div>
</div>
</div>
<div class="clear"></div>
</div>

<div class="index_contents ">
<div class="small_box MR9">
<h2><img src="<?php bloginfo('template_url'); ?>/images/index/img_06.jpg" alt="みやの動物病院診療時間" class="header"></h2><div class="small_box_inner">
<div class="middle_box_contents">
<div class="small_box_text">

<table cellpadding="0" cellspacing="0" class="time">
<tr>
<th>受付</th>
<th>月</th>
<th>火</th>
<th>水</th>
<th>木</th>
<th>金</th>
<th>土日祝</th>
</tr>
<tr>
<td>午前<br><span class="t9">9:00<br>12:00</span></td>
<td>●</td>
<td>●</td>
<td>●</td>
<td>●</td>
<td>●</td>
<td>●</td>
</tr>
<tr>
<td>午後<br><span class="t9">13:30<br>19:30</span></td>
<td>●</td>
<td>●</td>
<td>●</td>
<td>●</td>
<td>●</td>
<td>17：00<br>まで</td>
</tr>
</table>




</div>
</div>
</div>
</div>

<div class="small_box MR9">
<h2><img src="<?php bloginfo('template_url'); ?>/images/index/img_07.jpg" alt="能登診療所診療時間" class="header"></h2><div class="small_box_inner">
<div class="middle_box_contents">
<div class="small_box_text">
<table cellpadding="0" cellspacing="0" class="time">
<tr>
<th>受付</th>
<th>月</th>
<th>火</th>
<th>水</th>
<th>木</th>
<th>金</th>
<th>土日祝</th>
</tr>
<tr>
<td>午前<br><span class="t9">9:00<br>12:00</span></td>
<td>●</td>
<td>●</td>
<td>●</td>
<td>休診</td>
<td>●</td>
<td>●</td>
</tr>
<tr>
<td>午後<br><span class="t9">14:00<br>19:00</span></td>
<td>●</td>
<td>●</td>
<td>休診</td>
<td>休診</td>
<td>●</td>
<td>17：00<br>まで</td>
</tr>
</table>

<p>※院長の診察日は水曜日午前、土曜日の午後のみとなります。</p>
</div>
</div>
</div>
</div>

<div class="small_box">
<h2><img src="<?php bloginfo('template_url'); ?>/images/index/img_08.jpg" alt="院長のコラム" class="header"></h2><div class="small_box_inner">
<div class="middle_box_contents">
<div class="small_box_text">
<img src="<?php bloginfo('template_url'); ?>/images/index/img_09.jpg">
<p>みやの院長がペットについてのマメ知
識はもちろん、ペット以外の情報も
つづっていきます。</p>
<p class="right"><a href="http://miyano-animal.seesaa.net/"><img src="<?php bloginfo('template_url'); ?>/images/index/img_10.jpg" alt="コラム一覧"></a></p>
<div class="clear"></div>
</div>
</div>
</div>
</div>
<div class="clear"></div>
</div>


<div class="index_contents">

<div class="small_box MR9">

<h2><img src="images/index/staff_title.jpg" alt="みやの動物病院スタッフブログ" class="header"></h2>

<div class="small_box_inner">

<div class="middle_box_contents">

<div class="small_box_text">

<img src="images/index/staff_photo.jpg" width="195" height="112">

<?php RSSImport(3, "http://miyano-staff.seesaa.net/index.rdf",true,false); ?>


<p class="right"><a href="http://miyano-staff.seesaa.net/"><img src="images/index/btn_blog.jpg" alt="ご案内"></a></p>

<div class="clear"></div>

</div>

</div>

</div>

</div>

<div class="small_box MR9">

<h2><img src="images/index/noto_title.jpg" alt="能登診療所スタッフブログ" class="header"></h2>

<div class="small_box_inner">

<div class="middle_box_contents">

<div class="small_box_text">

<img src="images/index/noto_photo.jpg" width="195" height="112">

<?php RSSImport(3, "http://noto-staff.seesaa.net/index.rdf",true,false); ?>

<p class="right"><a href="http://noto-staff.seesaa.net/"><img src="images/index/btn_blog.jpg" alt="ご案内"></a></p>

<div class="clear"></div>

</div>

</div>

</div>

</div>


<div class="small_box">
<h2><img src="<?php bloginfo('template_url'); ?>/images/index/img_13.jpg" alt="アニマルビューティサロンマック" class="header"></h2>
<div class="small_box_inner heightLineParent">
<div class="middle_box_contents">
<div class="small_box_text">
<img src="<?php bloginfo('template_url'); ?>/images/index/img_14.jpg">
<p>当店は、みやの動物病院内に併設しており
「美容」と「健康」、そして「思いやり」が
テーマのワンちゃんネコちゃんの美容室
です。</p>
<p class="right"><a href="http://miyano-animal.com/salon/"><img src="<?php bloginfo('template_url'); ?>/images/index/img_15.jpg" alt="ご案内"></a></p>
<div class="clear"></div>
</div>
</div>
</div>
</div>







</div>













</div>



<?php get_footer(); ?>
