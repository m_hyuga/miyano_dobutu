<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="page" class="right">
  <div class="page_contents">
  <div class="page-Me">
<img src="<?php bloginfo('template_url'); ?>/images/page/commmon/img_01.jpg" alt="" width="226" height="28" onmouseover="this .src='<?php bloginfo('template_url'); ?>/images/page/commmon/img_01_on.jpg'" onmouseout="this .src='<?php bloginfo('template_url'); ?>/images/page/commmon/img_01.jpg'" >
 

  
  </div>
<div class="page-Con">
<div class="page-Con02">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div id="content" role="main">
<?php the_content(''); ?>
<br class="clear"><?php endwhile; endif; ?>

			</div>

</div>
</div>
<div class="clear"><img src="<?php bloginfo('template_url'); ?>/images/page/outline/img_13.jpg" width="677" height="22"></div>
</div>
</div>



<?php get_footer(); ?>
