<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<?php get_sidebar(); ?>

<div id="page" class="right">
  <div class="page_contents">
  <div class="page-Me">
  <ul>
  <li><a href="http://miyano-animal.com/salon/"><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_menu01_on.jpg" alt="mac" width="337" height="34" ></a></li>
  <li><a href="http://beautysalon-mac.seesaa.net/"><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_menu02.jpg" alt="スタッフブログ" width="341" height="34" onmouseover="this .src='http://miyano-animal.com/wp-content/themes/salon/images/index/img_menu02_on.jpg'" onmouseout="this .src='http://miyano-animal.com/wp-content/themes/salon/images/index/img_menu02.jpg'"></a></li>
   
 
  </ul>
  <br class="clear">

  
  </div>
<div class="page-Con">
<div id="content" role="main">
<div class="page-Con02">
<div class="page-Con02">
<div class="page_contentsL01">
<h3><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_title01.jpg" alt="ビューティサロンマック" width="250" height="45"></h3>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="TableA">
<tr>
<th>住所</th>

<td>〒920-8217 石川県金沢市近岡町410 </td>
</tr>
<tr>
<th>TEL</th>
<td>076-238-0056</td>

</tr>
<tr>
<th>FAX</th>
<td>076-238-0079</td>
</tr>
<tr>
<th>営業時間</th>
<td>平日・土：AM9：00～PM7：30<br>
日・祝祭日：PM6:00まで<br>上記以外の時間は応相談<br>※予約優先</td>
</tr>
<tr>
<th>動物取扱業登録証</th>
<td>保管：第106B003号<br />
動物取扱責任者：三崎　沙織<br />
有効期限：Ｈ28年7月2日</td>
</tr>
</table>
</div>



<div class="page_contentsR01">
<p><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_01.jpg" alt="" width="308" height="223"></p>



</div>

<p><br class="clear"></p>

</div>








<div class="page-Con02 MT30px">
<h3><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_title02.jpg" alt="スタッフ紹介" width="170" height="44"></h3>

<div class="NameBox">
<div class="NameBoxL">
<img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_staff01.jpg" width="114" height="116">
<p><span class="t10 MR5">店長</span>三崎　沙織</p>

</div>
<div class="NameBoxR">
お客様の大切なワンちゃんネコちゃんをきれいに、そしてより可愛くするお手伝いをさせて頂きます。ライフスタイル、個性を活かしたスタイルを提案していけたらと思います。<br />
犬・猫Loveな店長　三崎です♪


</div>

<br clear="all">

</div>


<br>
<div class="NameBox left MR10">
<div class="NameBoxL">
<img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_staff02.jpg" width="114" height="116">
<p>山田　梓</p>

</div>
<div class="NameBoxR">
「毎日明るく、元気に、笑顔で」をモットーに働いています。<br />
季節や環境に合わせるなど、飼い主様とワンちゃん・ネコちゃんが
毎日快適に過ごせるよう提案を心がけております。<br />
喜んで頂けるよう頑張ります。ご来店、お待ちしております。</div>

<br clear="all">

</div>



<div class="NameBox left">
<div class="NameBoxL">
<img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_staff03.jpg" width="114" height="116">
<p>加藤　亜弓</p>

</div>
<div class="NameBoxR">
可愛くするのはもちろん、お洋服を着ているワンちゃんは毛玉ができやすので
  スッキリカットしてお手入れしやすいようにするなど、お客様とワンちゃん達のライフタイルにあった
  カットを提供できるように頑張ります！<br />
  よろしくお願いします!!



</div>

<br clear="all">

</div>





<p><br class="clear"></p>
</div>

<div class="page-Con02 MT30px">
<h3><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_title03.jpg" alt="サロンメニュー" width="156" height="46"></h3>

<div class="SalonBox">
<div class="SalonBoxL">
<table cellpadding="0" cellspacing="0">

<thead>
<tr>
<th colspan="2"><strong>トリミング料金（予約制）</strong></th>
<td>シャンプーセット</td>
<td>シャンプー＆カット</td>
</tr>
</thead>
<tbody>
<tr>
<th colspan="2">チワワ・ミニピン</th>
<td>\3,000～</td>
<td>\4,000～</td>
</tr>

<tr>
<th rowspan="2">Mダックス</th>
<th>スムース</th>
<td>\3,500～</td>
<td></td>
</tr>
<tr>

<th>ロング</th>
<td>\3,500～</td>
<td>\4,500～</td>
</tr>

<tr>
<th colspan="2">シーズー・マル・ヨーキー</th>
<td>\2,500～</td>
<td>\4,500～</td>
</tr>

<tr>
<th colspan="2">ポメラニアン・ペキニーズ</th>
<td>\3,500～</td>
<td>\4,500～</td>
</tr>

<tr>
<th colspan="2">Mシュナ・テリア</th>
<td>\4,000～</td>
<td>\5,000～</td>
</tr>

<tr>
<th colspan="2">柴・コーギー・シェルティ</th>
<td>\4,500～</td>
<td>応相談</td>
</tr>

<tr>
<th colspan="2">プードル</th>
<td>\4,000～</td>
<td>\5,500～</td>
</tr>

<tr>
<th colspan="2">コッカー</th>
<td>\5,000～</td>
<td>\6,000～</td>
</tr>

<tr>
<th colspan="2">ラブラドールレトリバー</th>
<td>\6,500～</td>
<td></td>
</tr>

<tr>
<th colspan="2">ゴールデンレトリバー</th>
<td>\8,000～</td>
<td>\8,000～</td>
</tr>
<tr>
<th colspan="2">短毛猫</th>
<td>\3,500～</td>
<td></td>
</tr>
</tbody>
<tfoot>
<tr>
<th colspan="2">長毛猫</th>
<td>\4,500～</td>
<td>\4,500～</td>
</tr>


</tfoot>


</table>

<ul>
<li>●ワクチン接種をしているワンちゃん・ネコちゃんに限ります。
<br>　お預かりの際は、ワクチン接種の証明書をご持参願います。</li>
<li>●場合によっては、お断りさせていただく場合もありますので、
<br>　予めご了承ください。</li>
</ul>



</div>

<div class="SalonBoxR">
<table cellpadding="0" cellspacing="0">

<thead>
<tr>
<th colspan="2"><strong>お手入れ料金</strong></th>
</tr>
</thead>
<tbody>
<tr>
<th>お手入れセット<br>
<span class="t10">爪きり/耳掃除/肛門腺/足裏バリカン/足回りカット</span></th>
<td>
\1,800</td>
</tr>
<tr>
<th>足回りお手入れセット<br>
<span class="t10">爪きり/足裏バリカン/足回りカット</span></th>
<td>
\1,300～</td>
</tr>
<tr>
<th>爪きり</th>
<td>
\500</td>
</tr>

<tr>
<th>もつれ・抜け毛</th>
<td>
\500～</td>
</tr>

<tr>
<th>噛む・あばれる</th>
<td>
\1,000～</td>
</tr>

<tr>
<th>部分カット</th>
<td>
\500～</td>
</tr>

<tr>
<th>デザイン料<br>
<span class="t10">ブレスレット/バンド/カラーリング　etc</span></th>
<td>
\500～</td>
</tr>

<tr>
<th>のみ取り・ダニ取り</th>
<td>
\500～</td>
</tr>

<tr>
<th>薬用シャンプー</th>
<td>
\500～</td>
</tr>

<tr>
<th>ハミガキ<br>
<span class="t10">美容のお客様　￥500</span></th>
<td>
\1,000～</td>
</tr>

<tr>
<th>アンチエイジングカラー</th>
<td>
\3,000</td>
</tr>

<tr>
<th>泥パック</th>
<td>
\1,000～</td>
</tr>

<tr>
<th>トリートメント<br>
<span class="t10">イオンコース</span></th>
<td>
\500～</td>
</tr>
</tbody>
<tfoot>
<tr>
<th>送迎料金</th>
<td>
\1,000～</td>
</tr>
</tfoot>
</table>





</div>
<br clear="all">
</div>
</div>



<div class="HotelBox MT30px">
<table cellpadding="0" cellspacing="0">
<thead>
<tr>
<th>ペットホテル料金</th>
<td><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_small.jpg" alt="小型犬" width="50" height="66"></td>
<td><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_middle.jpg" alt="中型犬" width="66" height="68"></td>
<td><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_big.jpg" alt="大型犬" width="61" height="69"></td>
<td><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_more.jpg" alt="超大型犬" width="72" height="66"></td>
<td><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_cat.jpg" alt="猫" width="56" height="65"></td>
</tr>
</thead>
<tbody>
<tr>
<th>ホテル（一泊）</th>
<td>\3,000</td>
<td>\3,500</td>
<td>\4,000</td>
<td>\4,500</td>
<td>\2,500</td>
</tr>
</tbody>
<tfoot>
<tr>
<th>お預かり（1時間）</th>
<td>\300</td>
<td>\400</td>
<td>\500</td>
<td>\600</td>
<td>\400</td>
</tr>
</tfoot>

</table>

</div>

</div>

<div class="page-Con02 MT30px">
<h3><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_title04.jpg" alt="取り扱い商品" width="136" height="40"></h3>

<div class="ItemBox">
<table cellpadding="0" cellspacing="0" class="MT20px">
<tr>
<th><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_iphoto01.jpg" width="150" height="150"></th>
<td><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_item02.jpg" alt="フード取り扱いメーカー" width="123" height="39"><br>
DRS.CHOICE（犬・猫）<br>
ビューティプロ（犬・猫）<br>
ナチュラル・バランス（犬）</td>
<th><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_iphoto02.jpg"  width="150" height="150"></th>
<td><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_item01.jpg" alt="ウェア取り扱いメーカー" width="124" height="37"><br>
iDOG<br>
ポンポリース<br>
その他</td>
</tr>

<tr>
<th><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_iphoto03.jpg" width="150" height="150"></th>
<td><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_item03.jpg" alt="おやつ取り扱いメーカー" width="124" height="42"><br>
DRS.CHOICE（犬・猫）<br>
ビューティプロ（犬・猫）<br>
ナチュラル・バランス（犬）</td>
<th></th>
<td></td>
</tr>
</table>



</div>






</div>





</div>
</div>
<div class="clear"><img src="http://miyano-animal.com/wp-content/themes/salon/images/index/img_bottom.jpg" width="678" height="10"></div>
</div>
</div>



<?php get_footer(); ?>
