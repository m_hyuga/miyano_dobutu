<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>


<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>
<div class="clear"></div>
</div>


<!--footer-->
<div id="footer_wrapper">
<div id="footer">
<p class="left F_text01"><a href="http://miyano-animal.com/from/">お問い合わせ</a></p>
<p class="right"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/footer/img_top.jpg"></a></p>
<div class="clear"></div>
</div>
</div>





</div>
</div>
</body>
</html>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
