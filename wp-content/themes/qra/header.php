<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	?> ┃石川県の動物病院</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<!--<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/common.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/index.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/page.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/style.css" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
<div id="contener">
<div id="wrapper">

<!--header-->
<div id="header_wrapper">
<h1>みやの動物病院は石川県金沢市と石川県鳳珠郡穴水町にある動物病院です。</h1>
<div id="header">
<div class="left"><a href="http://miyano-animal.com/"><img src="<?php bloginfo('template_url'); ?>/images/header/img_01.jpg" alt="みやの動物病院"></a></div>
<div class="right"><img src="<?php bloginfo('template_url'); ?>/images/header/img_02.jpg" alt="石川県金沢市近岡町410 TEL.076-238-1321　FAX.076-238-0079
【能登診療所】 石川県鳳珠郡穴水町此木11-14-1 TEL&FAX0768-52-1001"></a></div>
<div class="clear"></div>
</div>
</div>




<!--navi-->
<div id="navi">
<ul>
<li><a href="http://miyano-animal.com/hospital"><img src="<?php bloginfo('template_url'); ?>/images/header/img_03.jpg" alt="みやの動物病院"></a></li>
<li><a href="http://miyano-animal.com/salon"><img src="<?php bloginfo('template_url'); ?>/images/header/img_04.jpg" alt="ビューティサロンマック"></a></li>
<li><a href="http://miyano-animal.com/faq"><img src="<?php bloginfo('template_url'); ?>/images/header/img_05.jpg" alt="よくある質問"></a></li>
<li><a href="http://miyano-animal.seesaa.net/"><img src="<?php bloginfo('template_url'); ?>/images/header/img_06.jpg" alt="院長コラム"></a></li>
</ul>
<div class="clear"></div>
</div>



<div id="contents">







