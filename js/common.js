/**********************************************************************

  カレンダー

**********************************************************************/

//本日のセル色を変更
function setCalendar() {
	var today = new Date();
	var year  = new String(today.getFullYear());
	var month = new String(today.getMonth() + 1);
	var date  = new String(today.getDate());

	while (month.length < 2) {
		month = '0' + month;
	}
	while (date.length < 2) {
		date = '0' + date;
	}

	var node_calendar_cel = document.getElementById('calendar_' + year + month + date);

	if (node_calendar_cel) {
		node_calendar_cel.className = 'today';
	}

	return;
}

/**********************************************************************

  処理開始

**********************************************************************/

//読み込み完了時
window.onload = function() {
	setCalendar();
};
